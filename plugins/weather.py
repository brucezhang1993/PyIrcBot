#!/usr/bin/env python3
# *-- coding: utf-8 --*

import requests
import json

def weather(args, nick, channel, c, e):
    args = args.split()
    if len(args) == 0:
        return "%s: 请输入城市" % nick
    if len(args) == 1:
        city = args[0].strip('市') + '市'
        location = _get_city_location(city)
    if len(args) == 2:
        city = args[0].strip('市') + '市'
        place = args[1]
        location = _get_city_location(city, place)
    if location is not False:
        (position, formatted_address) = location
        darksky_key = "c0930b16e7d43d90e3770bf306c25260"
        darksky_url = "https://api.darksky.net/forecast/%s/%s?lang=zh&units=si" % (darksky_key, position)
        con1 = requests.get(darksky_url)
        # print(con1.content)
        js1 = json.loads(con1.content)
        wind_grade, wind_state = _get_wind(js1['currently']['windSpeed'])
        return "%s: %s 即时天气 %s 实时温度 %.2f℃ 体感温度 %.2f℃ 降水概率 %d%% 湿度 %d%% 紫外线指数 %d 风速 %.2fm/s %d级(%s) 短时预报 %s 本周天气 %s" \
               % (nick, formatted_address, js1['currently']['summary'], js1['currently']['temperature'], js1['currently']['apparentTemperature'],
                  js1['currently']['precipProbability']*100, js1['currently']['humidity']*100, js1['currently']['uvIndex'], js1['currently']['windSpeed'], wind_grade, wind_state, js1['hourly']['summary'], js1['daily']['summary'])
    else:
        return "%s: 城市输入有误" % nick


def _get_city_location(city, place=None):
    if place:
        map_url = "https://restapi.amap.com/v3/geocode/geo?key=%s&address=%s&city=%s" % \
                ("3aa0d7cd316642f5484ecd4aa6886c91", place, city)
    else:
        map_url = "https://restapi.amap.com/v3/geocode/geo?key=%s&address=%s&city=%s" % \
                ("3aa0d7cd316642f5484ecd4aa6886c91", city, city)
    con = requests.get(map_url)
    js = json.loads(con.content)
    # print(js)
    if 'geocodes' in js.keys() and len(js['geocodes']) > 0:
        position_original = js['geocodes'][0]['location']
        position_list = position_original.split(',')
        position = position_list[1] + ',' + position_list[0]
        return position, js['geocodes'][0]['formatted_address']
    return False

def _get_wind(speed):
    state = (0, '无风')
    if speed >= 0.3:
        state = (1, '软风')
    if speed >= 1.6:
        state = (2, '轻风')
    if speed >= 3.4:
        state = (3, '微风')
    if speed >= 5.5:
        state = (4, '和风')
    if speed >= 8.0:
        state = (5, '轻劲风')
    if speed >= 10.8:
        state = (6, '强风')
    if speed >= 13.9:
        state = (7, '疾风')
    if speed >= 17.2:
        state = (8, '大风')
    if speed >= 20.8:
        state = (9, '烈风')
    if speed >= 24.5:
        state = (10, '狂风')
    if speed >= 28.5:
        state = (11, '暴风')
    if speed >= 32.7:
        state = (12, '台风/飓风')
    if speed >= 37.0:
        state = (13, '')
    return state
